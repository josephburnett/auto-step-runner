{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.com/josephburnett/auto-step-runner/schema/v1/definition",
    "oneOf": [
        {
            "required": [
                "steps"
            ],
            "title": "steps"
        },
        {
            "required": [
                "exec"
            ],
            "title": "exec"
        }
    ],
    "properties": {
        "steps": {
            "items": {
                "oneOf": [
                    {
                        "required": [
                            "step"
                        ],
                        "title": "step"
                    },
                    {
                        "required": [
                            "script"
                        ],
                        "title": "script"
                    },
                    {
                        "required": [
                            "action"
                        ],
                        "title": "action"
                    }
                ],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Name is a unique identifier for this step."
                    },
                    "step": {
                        "oneOf": [
                            {
                                "type": "string"
                            },
                            {
                                "properties": {
                                    "git": {
                                        "$schema": "https://json-schema.org/draft/2020-12/schema",
                                        "$id": "https://gitlab.com/josephburnett/auto-step-runner/schema/v1/git-reference",
                                        "properties": {
                                            "url": {
                                                "type": "string",
                                                "description": "Url is the location of the Git repo containing the step\ndefinition."
                                            },
                                            "dir": {
                                                "type": "string",
                                                "description": "Dir is the relative path to the step definition within the\nGit repo."
                                            },
                                            "rev": {
                                                "type": "string",
                                                "description": "Rev is the step version to use."
                                            }
                                        },
                                        "additionalProperties": false,
                                        "type": "object",
                                        "required": [
                                            "url",
                                            "dir",
                                            "rev"
                                        ],
                                        "description": "GitReference is a reference to a step in a Git repository containing the full set of configuration options."
                                    }
                                },
                                "type": "object"
                            }
                        ],
                        "description": "Step is a reference to the step to invoke."
                    },
                    "env": {
                        "additionalProperties": {
                            "type": "string"
                        },
                        "type": "object",
                        "description": "Env is a map of environment variable names to string values."
                    },
                    "inputs": {
                        "type": "object",
                        "description": "Inputs is a map of step input names to structured values."
                    },
                    "script": {
                        "type": "string",
                        "description": "Script is a shell script to evaluate."
                    },
                    "action": {
                        "type": "string",
                        "description": "Action is a GitHub action to run."
                    },
                    "if": {
                        "type": "string",
                        "description": "Let's go ahead and slam a few more things in here that are useful."
                    },
                    "steps": {
                        "items": true,
                        "type": "array"
                    },
                    "else": {
                        "items": true,
                        "type": "array"
                    }
                },
                "additionalProperties": false,
                "type": "object",
                "required": [
                    "name"
                ],
                "description": "Step is a single step invocation."
            },
            "type": "array",
            "description": "Steps is a list of sub-steps to run for the `steps` type."
        },
        "exec": {
            "properties": {
                "command": {
                    "items": {
                        "type": "string"
                    },
                    "type": "array",
                    "description": "Command are the parameters to the system exec API. It does not invoke a shell."
                },
                "work_dir": {
                    "type": "string",
                    "description": "WorkDir is the working directly in which `command` will be exec'ed."
                }
            },
            "additionalProperties": false,
            "type": "object",
            "required": [
                "command"
            ],
            "description": "Exec is a command to run for the `exec` type."
        },
        "outputs": {
            "type": "object",
            "description": "Outputs are the output values for a `steps` type. They can reference the outputs of sub-steps."
        },
        "env": {
            "additionalProperties": {
                "type": "string"
            },
            "type": "object",
            "description": "Env is a map of environment variable names to values for all steps"
        }
    },
    "additionalProperties": false,
    "type": "object",
    "description": "Definition is the implementation of a step."
}