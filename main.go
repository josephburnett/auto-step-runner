package main

import (
	"os"

	"gitlab.com/josephburnett/auto-step-runner/cmd"
	"gitlab.com/josephburnett/auto-step-runner/cmd/ci"
)

func main() {
	cmd.RootCmd.AddCommand(ci.Cmd)
	err := cmd.RootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
