syntax = "proto3";
package proto;

import "google/protobuf/struct.proto";
import "buf/validate/validate.proto";

option go_package = "../proto";

message Step {
    string name = 1; 
    Reference step = 2;
    map<string,string> env = 3;
    option (buf.validate.message).cel = {
        id: "env",
        message: "env must be alphanumeric with underscores",
        expression: "this.env.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
    };
    map<string,google.protobuf.Value> inputs = 4;
    option (buf.validate.message).cel = {
        id: "inputs",
        message: "inputs must be alphanumeric with underscores",
        expression: "this.inputs.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
    };
    message Reference {
        StepReferenceProtocol protocol = 1;
        string url = 2;
        repeated string path = 3;
        string filename = 4;
        string version = 5;
    }
}

enum StepReferenceProtocol {
    step_reference_protocol_unspecified = 0;
    local = 1;
    git = 2;
    zip = 3;
    oci = 4;
}

message Definition {
    DefinitionType type = 1;
    Exec exec = 2;
    repeated Step steps = 3;
    message Exec {
        repeated string command = 1;
        string work_dir = 2;
    }
    map<string,google.protobuf.Value> outputs = 4;
    option (buf.validate.message).cel = {
        id: "outputs",
        message: "outputs must be alphanumeric with underscores",
        expression: "this.outputs.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
    };
    map<string,string> env = 5;
}

enum DefinitionType {
    definition_type_unspecified = 0;
    exec = 1;
    steps = 2;
}

message Spec {
    Content spec = 1;
    message Content {
        map<string,Input> inputs = 1;
        option (buf.validate.message).cel = {
            id: "inputs",
            message: "inputs must be alphanumeric with underscores",
            expression: "this.inputs.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
        };
        message Input {
            ValueType type = 1;
            google.protobuf.Value default = 2;
        }
        map<string,Output> outputs = 2;
        option (buf.validate.message).cel = {
            id: "outputs",
            message: "outputs must be alphanumeric with underscores",
            expression: "this.outputs.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
        };
        message Output {  
            ValueType type = 1;
            google.protobuf.Value default = 2;
        }
    }
}

message StepDefinition {
    Spec spec = 1;
    Definition definition = 2;
    string dir = 3;
}

enum ValueType {
    value_type_unspecified = 0;
    raw_string = 1;
    string = 2;
    number = 3;
    boolean = 4;
    struct = 5;
    array = 6;
    step_result = 7;
}

message StepResult {
    Step step = 1;
    StepDefinition stepDefinition = 2;
    enum Status {
        unspecified = 0;
        running = 1;
        success = 2;
        failure = 3;
    }
    Status status = 4;
    map<string,google.protobuf.Value> outputs = 5;
    option (buf.validate.message).cel = {
        id: "outputs",
        message: "outputs must be alphanumeric with underscores",
        expression: "this.outputs.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
    };
    map<string,string> exports = 6;
    option (buf.validate.message).cel = {
        id: "exports",
        message: "exports must be alphanumeric with underscores",
        expression: "this.exports.all(key, key.matches('^[a-zA-Z_][a-zA-Z0-9_]*$'))",
    };
    int32 exit_code = 7;
    repeated StepResult children_step_results = 8;
}
service StepRunner {
    rpc Run(RunRequest) returns (RunResponse);
}

message Variable {
    string key = 1;
    string value = 2;
    bool file = 3;
    bool masked = 4;
}

message Job {
    repeated Variable variables = 1;
    string build_dir = 4;
}

message RunRequest {
    string id = 1;
    string work_dir = 2;
    map<string,string> env = 3;
    Job job = 5;
    string steps = 6;
}

message RunResponse {
}
